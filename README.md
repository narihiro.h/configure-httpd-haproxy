# configure-httpd-haproxy

### Description
---

This playbook executes the following tasks.

- launch containers
- install packages
- configure httpd, haproxy
- perform smoke test by accessing  HAProxy VIP

### Test environment
---

- LXD Host : Ubuntu 20.04 (LXD)
- LXD Container: CentOS8
- ansible 2.9.12

### How to run
---

```
$ ansible-playbook -i hosts/inventory.ini site.yml
```
